# # 导入SQLAlchemy部件
# from sqlalchemy import create_engine
# from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import sessionmaker
# # 连接MySQL数据库需要导入pymysql模块
# import pymysql
#
# pymysql.install_as_MySQLdb()
#
# # 为SQLAlchemy 定义数据库URL地址
# # 配置数据库地址:数据库类型+数据库驱动名称：//用户名：密码@机器地址：端口号/数据库名
# SQLALCHEMY_DATABASE_URL = 'mysql://root:gd_654321@124.223.200.220:3306/fastapi_hanlp?charset=utf8mb4'
#
# # 创建SQLAlchemy引擎
# engine = create_engine(SQLALCHEMY_DATABASE_URL)
#
# # 创建数据库会花
# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
#
# # 创建一个Base类declarative_base
# Base = declarative_base()
