from server import HanlpNativeServer, HanlpRestFulServer, HanlpNativeStsServer
import json


# Hanlp2.0 本地化服务
def hanlpNativeService(businessType: str, text: str):
    if businessType == "tok/fine":
        return json.dumps(HanlpNativeServer.tokFine(text))
    if businessType == "tok/coarse":
        return json.dumps(HanlpNativeServer.tokCoarse(text))


# Hanlp2.0 文本相似度
def hanlpNativeStsService(firsText: str, secondText: str):
    list = [(firsText, secondText)]
    return json.dumps(HanlpNativeStsServer.hanlp_sts_native(list))


# Hanlp2.0Restful服务
def hanlpRestfulService(businessType: str, text: str):
    if businessType == "tok/fine":
        return json.dumps(HanlpRestFulServer.tokenize(text, False))
    if businessType == "tok/coarse":
        return json.dumps(HanlpRestFulServer.tokenize(text, True))
    if businessType == "key/extra":
        return json.dumps(HanlpRestFulServer.hanlpKEY(text))
    if businessType == "sentiment":
        return json.dumps(HanlpRestFulServer.hanlpSA(text))
