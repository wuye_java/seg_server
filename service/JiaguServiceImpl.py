from server import JiaguServer
import json


def JiaguService(businessType: str, text: str, keynum: int):
    # 分词
    if businessType == "seg":
        return json.dumps(JiaguServer.jiaguSeg(text))
    # 词性标注
    if businessType == "pos":
        return json.dumps(JiaguServer.jiaguPos(text))
    # 命名实体识别
    if businessType == "ner":
        return json.dumps(JiaguServer.jiaguNer(text))
    # 关键词提取
    if businessType == "keywords":
        return json.dumps(JiaguServer.jiaguKeywords(text, keynum))
    # 文本摘要
    if businessType == "summarize":
        return json.dumps(JiaguServer.jiaguSummarize(text, 1))
    # 情感分析
    if businessType == "sentiment":
        return json.dumps(JiaguServer.jiaguSentiment(text))
    # 文本聚类
    if businessType == "cluster":
        return json.dumps(JiaguServer.jiaguText_Cluster(text))
