import uvicorn
from fastapi import FastAPI
from api import HanlpApi as hanlp, JiaguApi as jiagu, JieBaApi as jieba
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from datetime import datetime
# from pc.JinRiExporter import JinRipc, JinRiweb

app = FastAPI()

# Hanlp 分词服务
app.include_router(hanlp.router)

# jiagu 分词服务
app.include_router(jiagu.router)

# jieba 分词服务
app.include_router(jieba.router)

# 爬虫定时任务

# scheduler = AsyncIOScheduler()


#
# @scheduler.scheduled_job('interval', minutes=0, hour=0)
# async def cron_job():
#     JinRipc()
#     JinRiweb()


# 启动scheduler
# @app.on_event("startup")
# async def startup_event():
#     scheduler.start()
#
#
# @app.on_event("shutdown")
# async def shutdown_event():
#     scheduler.shutdown()


if __name__ == "__main__":
    uvicorn.run(app='start:app', host='0.0.0.0', port=8000, reload=True, debug=True)
