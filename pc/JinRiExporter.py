# import time
#
# import requests
# from bs4 import BeautifulSoup
# from sqlalchemy.orm import Session
#
# from pc.model import MessageModel
# from pc.model import JinRiModel
#
# from db.database import SessionLocal
#
# list = []
#
#
# def get_db():
#     db = SessionLocal()
#
#     try:
#         return db
#     except Exception as e:
#         print(f"An error occurred: {e}")
#     finally:
#         db.close()
#
#
# def JinRiweb():
#     try:
#         # 设置请求头
#         headers = {
#             'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
#             'Content-Type': 'application/x-www-form-urlencoded'
#         }
#
#         # 创建POST数据
#         data = {'c': 'hot', 't': 'daily'}
#
#         # 发送POST请求
#         response = requests.post("https://tophub.today/do", data=data, headers=headers)
#
#         if response.status_code == 200:
#             # 解析JSON响应
#             json_data = response.json()
#             data_array = json_data['data']
#
#             for item in data_array:
#                 # 将JSON对象转为 MessageEntity 对象
#                 entity = JinRiModel.JinRiModel(**item)
#                 entity.type = "接口拉取"
#                 list.append(entity)
#                 insert_jinri(get_db(), entity)
#         else:
#             print(f"Failed to fetch the data. Status code: {response.status_code}")
#     except Exception as e:
#         print(f"An error occurred: {e}")
#
#     return list
#
#
# def JinRipc():
#     try:
#         headers = {
#             'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
#         }
#
#         # 发送HTTP GET请求，获取网页内容，添加Headers
#         url = 'https://tophub.today/'
#         response = requests.get(url, headers=headers)
#
#         if response.status_code == 200:
#             # 使用BeautifulSoup解析网页内容
#             soup = BeautifulSoup(response.text, 'html.parser')
#
#             # 找到所有带有class="cc-cd-cb-ll"的div元素
#             divs = soup.find_all('div', class_='cc-cd-cb-ll')
#
#             for div in divs:
#                 # 创建 MessageEntity 对象并填充数据
#                 entity = JinRiModel.JinRiModel()
#                 # Use .text to get only the text content without HTML tags
#                 entity.title = div.find_all('span', class_='t')[0].text.strip() if div.find_all('span', class_='t') else ''
#                 entity.description = div.find_all('span', class_='e')[0].text.strip() if div.find_all('span', class_='e') else ''
#                 entity.type = "网页爬虫"
#                 list.append(entity)
#                 insert_jinri(get_db(), entity)
#
#             # 添加等待时间，以降低请求频率
#             time.sleep(2)
#         else:
#             print(f"Failed to fetch the webpage. Status code: {response.status_code}")
#     except Exception as e:
#         print(f"An error occurred: {e}")
#
#     return list
#
#
# def insert_jinri(db: Session, jinri: JinRiModel):
#     db.add(jinri)
#     db.commit()
#     db.reset()
#
# # JinRiweb()
# JinRipc()
