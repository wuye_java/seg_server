# from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
# from sqlalchemy.orm import relationship
#
# from db.database import Base
#
#
# class JinRiModel(Base):
#     __tablename__ = "jinri"
#
#     idd = Column(Integer, primary_key=True, index=True)
#     ID = Column(String)
#     description = Column(String, unique=True, index=True)
#     domain = Column(String)
#     extra = Column(String)
#     logo = Column(String)
#     md5 = Column(String)
#     nodeids = Column(String)
#     sitename = Column(String)
#     thumbnail = Column(String)
#     time = Column(String)
#     title = Column(String)
#     topicid = Column(Integer)
#     url = Column(String)
#     views = Column(String)
#     type = Column(String)
#
#
# entity = JinRiModel(
#     ID="1",
#     description="Description",
#     domain="example.com",
#     extra="Extra",
#     logo="logo.jpg",
#     md5="md5hash",
#     nodeids="node1",
#     sitename="Site Name",
#     thumbnail="thumbnail.jpg",
#     time="2023-10-30",
#     title="Title",
#     topicid=123,
#     url="https://example.com",
#     views="1000",
#     type="网页"
# )