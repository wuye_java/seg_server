import time

from pc import JinRiExporter as jinri
from hanlp_restful import HanLPClient


def getJinRiData():
    webDataList = jinri.JinRiweb()
    pcDatalist = jinri.JinRipc()
    result = webDataList + pcDatalist
    return result


def getHanlpData(data):
    result = []  # 初始化为空列表

    HanLP = HanLPClient('https://www.hanlp.com/api', auth="MzMwOUBiYnMuaGFubHAuY29tOndoYVhWYWExdzUxcjB6Q1k=",
                        language='zh')
    for item in data:
        keywords = HanLP.keyphrase_extraction(item.title, topk=100)  # 提取关键词
        print(item.title + ": ")
        for key in keywords:
            print(key)
        time.sleep(2)

    print(result)
    return "result"
