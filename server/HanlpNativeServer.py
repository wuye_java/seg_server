import hanlp

# 多任务模型
HanLP = hanlp.load(hanlp.pretrained.mtl.CLOSE_TOK_POS_NER_SRL_DEP_SDP_CON_ELECTRA_SMALL_ZH, devices=-1)  # 世界最大中文语料库

# 单任务模型
# HanLP = hanlp.pipeline() \
#     .append(hanlp.utils.rules.split_sentence, output_key='sentences') \
#     .append(hanlp.load('FINE_ELECTRA_SMALL_ZH'), output_key='tok') \
#     .append(hanlp.load('CTB9_POS_ELECTRA_SMALL'), output_key='pos') \
#     .append(hanlp.load('MSRA_NER_ELECTRA_SMALL_ZH'), output_key='ner', input_key='tok') \
#     .append(hanlp.load('CTB9_DEP_ELECTRA_SMALL', conll=0), output_key='dep', input_key='tok')\
#     .append(hanlp.load('CTB9_CON_ELECTRA_SMALL'), output_key='con', input_key='tok')

# 中文分词 细粒度
def tokFine(txt: str):
    print(HanLP(txt, tasks='tok/fine'))
    return HanLP(txt, tasks='tok/fine')


# 中文分词 粗粒度
def tokCoarse(txt: str):
    return HanLP(txt, tasks='tok/coarse')


# 词性标注 CTB
def posCtb(txt: str):
    return HanLP(txt, tasks='pos/ctb')


# 词性标注 PKU
def posPku(txt: str):
    return HanLP(txt, tasks='pos/pku')


# 词性标注 863
def pos863(txt: str):
    return HanLP(txt, tasks='pos/863')


# 命名实体识别 MSRA
def nerMsra(txt: str):
    return HanLP(txt, tasks='ner/msra')


# 命名实体识别 PKU
def nerPku(txt: str):
    return HanLP(txt, tasks='ner/pku')


# 命名实体识别 OntoNotes
def nerOntonotes(txt: str):
    return HanLP(txt, tasks='ner/ontonotes')


# 语义角色标注
def srl(txt: str):
    return HanLP(txt, tasks='srl')


# 依存句法分析
def dep(txt: str):
    return HanLP(txt, tasks='dep')


# 语义依存分析
def sdp(txt: str):
    return HanLP(txt, tasks='sdp')


# 成分句法分析
def con(txt: str):
    return HanLP(txt, tasks='con')
