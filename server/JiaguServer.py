import jiagu


# 分词
def jiaguSeg(text):
    return jiagu.seg(text)


# 词性标注
def jiaguPos(text):
    return jiagu.pos(jiagu.seg(text))


# 命名实体识别
def jiaguNer(text):
    return jiagu.ner(jiagu.seg(text))


# 加载自定义字典，支持字典路径、字典列表形式
def jiaguLoad_userdict(any):
    jiagu.load_userdict(any)


# 知识图谱关系提取
def jiaguKnowledge(text):
    return jiagu.knowledge(text)


# 关键词提取
def jiaguKeywords(text, index):
    return jiagu.keywords(text, index)


# 文本摘要
def jiaguSummarize(text, index):
    return jiagu.summarize(text, index)


# 情感分析
def jiaguSentiment(text):
    return jiagu.sentiment(text)


# 文本聚类
def jiaguText_Cluster(docs):
    return jiagu.text_cluster(docs)
