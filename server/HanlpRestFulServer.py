# Hanlp 分词服务
from hanlp_restful import HanLPClient
from IPython.display import SVG, display
import hanlp

HanLP = HanLPClient('https://www.hanlp.com/api', auth='MzMwOUBiYnMuaGFubHAuY29tOndoYVhWYWExdzUxcjB6Q1k=', language='zh')


# 中文分词
# text:文本
# coarse:粒度 true粗粒度 反之细粒度
# 细粒度适合搜索引擎业务，粗粒度适合文本挖掘业务
def tokenize(text, coarse):
    result = []
    if coarse:
        result = HanLP.tokenize(text, coarse)
    else:
        result = HanLP.tokenize(text)
    return result


# 词性标注
# 标注句子中每个单词的词性的任务
# CTB：2.0版本默认采用 pos/ctb
# PKU：1.0版本默认采用 pos/pku
# 863：国家标准，诞生于863工程中，被国家语委语料库采用 pos/863
def hanlpPOS(text, tasks):
    return HanLP.parse(text, tasks=tasks).pretty_print()


# 命名实体识别
# 识别文本中实体的位置以及类别的任务
# PKU   1.0版本默认，国内最流行的标注集
# MSRA  最大的中文命名实体识别语料库
# OntoNotes 国际上最通用的标注规范
def hanlpNER(text, tasks):
    return HanLP.parse(text, tasks=tasks).pretty_print()


# 依存句法分析
# 分析一个句子中单词与单词之间的语法关系，并将其表示为树形结构的任务。
# SD
# UD
# PMT
def hanlpDEP(text):
    return HanLP.parse(text, tasks='dep')


# 依存句法可视化
def hanlpDEPPrettyPrint(text):
    HanLP.parse(text, tasks='dep').pretty_print()


# CoNLL格式
def hanlpDEEPCoNLL(text):
    return HanLP.parse(text, tasks='dep').to_conll()


# 成分句法分析
# 分析一个句子在语法上的递归构成，并将其表示为树形结构的任务
# CTB
# PTB
# NPCMJ
def hanlpCON(text):
    return HanLP.parse(text, tasks=['pos', 'con'])


# 成分句法可视化
def hanlpCONPrettyPrint(text):
    HanLP.parse(text, tasks=['pos', 'con']).pretty_print()


# 成分句法括号形式
def hanlpCONBrackets(text):
    doc = HanLP.parse(text, tasks=['pos', 'con'])
    return doc['con'][0]


# 语义依存分析
# 分析一个句子中单词与单词之间的语义关系，并将其表示为图结构的任务。
def hanlpSDP(text):
    return HanLP.parse(text, tasks='sdp')


# 语义依存可视化
def hanlpCoNll(text):
    return HanLP.parse(text, tasks='sdp').to_conll()


# 语义角色标注
def hanlpSRL(text):
    return HanLP.parse(text, tasks=['srl'])


# 语义角色标注可视化
def hanlpSRLPrettyPrint(text):
    HanLP.parse(text, tasks=['srl']).pretty_print()


# 抽象意义表示
# 将句子的意义(时间地点谁对谁怎样的做了什么)表示为以概念为节点的单源有向无环图的语言学框架
def hanlpAMR(text):
    return HanLP.abstract_meaning_representation(text)


# 抽象意义可视化——获得矢量图可视化
def hanlpAMR_SVG(text):
    graph = HanLP.abstract_meaning_representation(text, visualization='svg')[0]
    display(SVG(data=graph['svg']))


# 指代消解
# 将文本中指代同一事物的提名聚集到一个簇
def hanlpCOR(text):
    return HanLP.coreference_resolution(text)


# 语义文本相似度
# 判断一堆短文本的语义相似度，相似度区间为[0,1]
def hanlpSTS(dict):
    return HanLP.semantic_textual_similarity(dict)


# 文本风格转换
# 将原文本的风格转换为目标风格，同时保持语义不变
def hanlpTST(dict):
    return HanLP.text_style_transfer(dict, target_style='gov_doc')


# 关键词短语提取
# 文本中最具有代表性的关键词以及短语
def hanlpKEY(text):
    return HanLP.keyphrase_extraction(text)


# 抽取式自动摘要
# 从文章中筛选出一些作为摘要的中心句子
def hanlpES(text, topk):
    return HanLP.extractive_summarization(text, topk=topk)


# 生成式自动摘要
# 为文章生成一段简短的概括性摘要，生成的摘要有可能出现原文中不存在的新短语或新句子，并且整体流畅性较高
def hanlpAS(text):
    return HanLP.abstractive_summarization(text)


# 文本纠错
# 改正文本中潜在的拼写、标点、语法等表达错误
# 目前暂时仅支持拼写、标点和简单的语法错误
def hanlpGEC(dict):
    return HanLP.grammatical_error_correction(dict)


# 文本分类
# 判断一段文本所属的类别。
# 返回值为文章最可能得类别，可以通过topk和prob参数来输出前k个类别以及相应的概率
def hanlpTC(text):
    return HanLP.text_classification(text, model='news_zh')


# 情感分析
# 判断一段文本的情感极性。情感极性为[-1,1]之间的数值，数值的正负代表正负情绪，数值的绝对值代表情感的强烈程度
def hanlpSA(text):
    return HanLP.sentiment_analysis(text)
