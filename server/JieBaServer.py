import sys

import paddle
import jieba

import paddle.fluid

paddle.fluid.install_check.run_check()

paddle.enable_static()

jieba.enable_paddle()


# jieba分词（基于paddle）
def Jieba_lcut(text: str):
    return jieba.lcut(text, use_paddle=True)


# if __name__ == "__main__":
#     a = (sys.argv[1])
#     s = Jieba_lcut(a)
#     print("finish!!!")
#     print(s)

text = '飞桨（PaddlePaddle）以百度多年的深度学习技术研究和业务应用为基础，集核心框架、基础模型库、端到端开发套件、丰富的工具组件、星河社区于一体，是中国首个自主研发、功能丰富、开源开放的产业级深度学习平台。飞桨在业内率先实现了动静统一的框架设计'
seg_1 = jieba.cut(text, cut_all=False)
seg_2 = jieba.cut(text, use_paddle=True)

print('精 确 模 式：', "/".join(seg_1))
print('paddle模式：', "/".join(seg_2))
