import hanlp

hanlp.pretrained.sts.ALL

sts = hanlp.load(hanlp.pretrained.sts.STS_ELECTRA_BASE_ZH)


# 文本相似度
def hanlp_sts_native(sentences):
    resultList = sts(sentences)
    return resultList
