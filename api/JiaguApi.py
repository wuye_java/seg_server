from fastapi import APIRouter, Depends, HTTPException
from service import JiaguServiceImpl
from pydantic import BaseModel
from fastapi.responses import JSONResponse
import json

router = APIRouter(
    prefix="/jiagu",
    tags=["jiagu"],
)


class Search(BaseModel):
    businessType: str
    text: str
    keynum: int


# jiagu服务
@router.post("/jiaguService/")
async def jiaguService(search: Search):
    result = JiaguServiceImpl.JiaguService(search.businessType, search.text, search.keynum)
    return JSONResponse(content=json.loads(result))
