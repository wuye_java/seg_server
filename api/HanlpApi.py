from fastapi import APIRouter, Depends, HTTPException
from service import HanlpServiceImpl
from pydantic import BaseModel
from fastapi.responses import JSONResponse
import json

router = APIRouter(
    prefix="/hanlp",
    tags=["hanlp"],
)


class Search(BaseModel):
    businessType: str
    text: str


class Compare(BaseModel):
    firstText: str
    secondText: str


# hanlp2.0native服务
@router.post("/hanlpNativeService/")
async def hanlpNativeService(search: Search):
    result = HanlpServiceImpl.hanlpNativeService(search.businessType, search.text)
    return JSONResponse(content=json.loads(result))


@router.post("/hanlpNativeSTSService/")
async def hanlpNativeSTSService(compare: Compare):
    result = HanlpServiceImpl.hanlpNativeStsService(compare.firstText, compare.secondText)
    return JSONResponse(content=json.loads(result))
