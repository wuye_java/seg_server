from fastapi import APIRouter, Depends, HTTPException
from service import JieBaServiceImpl
from pydantic import BaseModel
from fastapi.responses import JSONResponse
import json

router = APIRouter(
    prefix="/jieba",
    tags=["jieba"],
)


class Search(BaseModel):
    businessType: str
    text: str


# jieba服务
@router.post("/jiebaService/")
async def jiebaService(search: Search):
    result = JieBaServiceImpl.JieBa(search.text)
    return JSONResponse(content=json.loads(result))
